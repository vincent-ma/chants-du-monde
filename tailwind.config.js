const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./dist/*.html"],
  theme: {
    extend: {
      screens: {
        "3xl": "1600px",
        "4xl": "2000px",
        "5xl": "2300px",
      },
    },
    colors: {
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      primary: {
        DEFAULT: "#FFB300",
        light: "#FFE19A",
        dark: "#D49500",
        darker: "#553C00",
      },
      green: {
        DEFAULT: "#82AF22",
        light: "#96C92B",
        dark: "#4A6219",
      },
      background: {
        DEFAULT: "#F4F1EA",
        secondary: "#DBD1C2",
      },
      brown: {
        DEFAULT: "#663C25",
        light: "#793E1E",
        darker: "#432312",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
