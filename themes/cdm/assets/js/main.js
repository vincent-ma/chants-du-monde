document.addEventListener("DOMContentLoaded", function () {
  let scroll_limit = 25;

  body_scroll_class();
  document.addEventListener("scroll", function () {
    body_scroll_class();
  });

  function body_scroll_class() {
    let pos = window.scrollY;
    if (pos > scroll_limit && !document.body.classList.contains("scrolled")) {
      document.body.classList.add("scrolled");
    }
    if (pos <= scroll_limit && document.body.classList.contains("scrolled")) {
      document.body.classList.remove("scrolled");
    }
  }

  document.querySelector("#menu_trigger").addEventListener("click", () => {
    document.body.classList.add("opened_menu");
  });

  document.querySelectorAll(".close_menu, .main_nav a").forEach((e) => {
    e.addEventListener("click", () => {
      document.body.classList.remove("opened_menu");
    });
  });

  document.querySelectorAll(".video_popup_link").forEach((e) => {
    e.addEventListener("click", () => {
      let link = e.attributes["data-link"].value;
      console.log(link);
      let iframe = document.createElement("iframe");

      iframe.setAttribute("src", link);
      iframe.setAttribute("frameBorder", 0);

      let video_view = document.getElementById("video_view");
      document.body.classList.add("has_video");
      video_view.append(iframe);
    });
  });

  function close_iframe() {
    let iframe = document.querySelector("#video_view iframe");
    if (iframe != null) {
      iframe.remove();
      document.body.classList.remove("has_video");
    }
  }

  let video_view = document.getElementById("video_view");
  let close = document.querySelector("#video_view .close");
  close.addEventListener("click", () => {
    close_iframe();
  });

  video_view.addEventListener("click", function (e) {
    if (e.target !== this) {
      return;
    }
    close_iframe();
  });

  function close_popup(e) {
    document.body.classList.remove("has_popup");
    e.classList.remove("is_active");
  }

  function open_popup(id) {
    document.body.classList.add("has_popup");
    document.getElementById(id).classList.add("is_active");
  }

  document.querySelectorAll("[data-popup_link]").forEach((e) => {
    e.addEventListener("click", () => {
      let target = e.getAttribute("data-popup_link");
      open_popup(target);
    });
  });

  document.querySelectorAll(".popup").forEach((e) => {
    e.addEventListener("click", function (i) {
      if (i.target !== this) {
        return;
      }
      close_popup(e);
    });
  });
  document.querySelectorAll(".popup .close").forEach((e) => {
    e.addEventListener("click", function (i) {
      let parent = e.parentElement.parentElement;
      close_popup(parent);
    });
  });
});
