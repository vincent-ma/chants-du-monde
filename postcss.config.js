const tailwindcss = require("tailwindcss");
module.exports = {
  plugins: {
    "postcss-preset-env": {},
    "postcss-import": {
      path: ["themes/cdm/assets/css/"],
    },
    "postcss-each": {
      plugins: {
        beforeEach: [
          require("postcss-nested"),
          require("tailwindcss"),
          require("autoprefixer"),
        ],
      },
    },
  },
};
