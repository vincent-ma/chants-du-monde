---
title: Page en Anglais
hero:
  nav:
    section_id: top
    text: Top
  title: A﻿ccess tutorials for **sacred songs of the World, Guitar,** and
    **Voice** exercices
  text: Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi
    obcaecati nostrum impedit non rem architecto aut, modi ad cupiditate tempora
    aperiam sint maiores autem repellat voluptas, repellendus nobis alias omnis.
  image: img/pjkbmdwz-1.png
  button_bar:
    - type: external_link
      style: green
      to: patreon
      text: support me on Patreon
    - type: internal_scroll
      style: outline
      text: watch Introduction
      section_id: introduction
      icon: play
blocks:
  - type: section
    title: Learn
    image: img/hf2b4cfa-1.jpg
    text: |-
      * **many songs** and **guitar accompaniment**
      * The **different song chords** from different keys
      * To **lead a bhajan**
      * The meaning of the lyrics
      * Specific **guitar rhythms** to accompany bhajans
    button_bar:
      - type: internal_scroll
        section_id: concept
        text: find out more
        style: green
    nav:
      section_id: learn
      text: Learn...
    invert: true
  - type: video
    title: Introduction video
    link: https://youtu.be/wKBdhskHUko
    image: img/maxresdefault.jpg
    nav:
      section_id: introduction
      text: Introduction video
  - type: section
    title: Concept Explanation
    image: img/zn38h7ea-1.png
    text: >-
      Minima ipsa, and fugiat, yet vitae. Duis cupidatat, and nequeporro. Sunt
      ratione, or ea, but in mollit fugit. Ut odit dolorem sunt, and fugit.
      Autem ad quae, but quaerat, and explicabo, for exercitation.\

      \

      Nequeporro id ullam, or ut, but aliquam elit, so dicta. Dicta aliquip. Lorem exercitationem, nor magna quaerat ab. Eaque. Error dolor, nesciunt. Natus. Elit ipsam, nor inventore si, so ratione, for consequat, or dicta.\

      \

      Aliquid sequi. Numquam. Unde. Aliquip numquam deserunt natus. Autem adipisicing, for incididunt, and iste, but eos, quaerat tempora. Eos. Quaerat fugit, or est, and duis, for ullamco. Vitae eaque, for unde, nor mollit. Nihil commodi explicabo commodo exercitationem. Ullamco quis. Quae. Adipisicing. Minima adipisci odit, nor dolore qui autem. Nequeporro dolores, for amet.
    nav:
      section_id: concept
      text: Concept Explanation
    button_bar:
      - type: external_link
        style: green
        to: patreon
        text: support me on Patreon
  - type: video
    title: Tutorial video
    link: https://youtu.be/M-lKuAMzNkw
    image: img/maxresdefault2.jpg
    nav:
      section_id: tuto
      text: Tutorial video
  - type: audio
    code: '<iframe width="100%" height="166" scrolling="no" frameborder="no"
      allow="autoplay"
      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243782397&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div
      style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break:
      normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;
      font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida
      Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a
      href="https://soundcloud.com/florian-surya-ananda" title="Florian Surya
      Ananda" target="_blank" style="color: #cccccc; text-decoration:
      none;">Florian Surya Ananda</a> · <a
      href="https://soundcloud.com/florian-surya-ananda/hari-om-tat-sat-nama-shivaya"
      title="hari om tat sat nama shivaya" target="_blank" style="color:
      #cccccc; text-decoration: none;">hari om tat sat nama shivaya</a></div>'
    title: Listen to "hari om tat sat nama shivaya" on Soundcloud
    nav:
      section_id: hari-om-tat-sat-nama-shivaya
      text: Listen to "hari om tat sat nama shivaya" on Soundcloud
  - type: testimonials
    testimonials:
      - text: >-
          A great moment of joy and fraternal sharing, full of fire which
          revives our heart and makes us want to spread it.


          Surya puts all his devotional energy at the service of singing and it vibrates strongly!


          Sharing such beautiful vibrations in a circle makes the heart celebrate.
        name: Sylvie
      - text: >-
          To meet to sing the Sacred, to listen, to be crossed by the vibrations
          of the sounds coming from the heart.


          Connect to the Divine to the point of ecstasy sometimes.


          I leave filled with the joy, playfulness and solar energy of Surya who gives his all and with great generosity in his circles.
        name: Carla
      - text: >-
          The Bhajans offered by Florian are a wonderful opportunity for songs
          and collective connections.


          Singing together and vibrating with a thousand fires a joy and a love in different forms, reminding us that there is an eternal space of joy and that this space is always in each of us.


          Gratitude for your invitations, Flow.
        name: Yannis
    nav:
      section_id: testimonials
      text: Testimonials
    title: Testimonials
---
