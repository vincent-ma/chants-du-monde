---
title: Page en Français
hero:
  title: Accédez à des tutos de **chants sacrés du monde**, de **guitare** et
    exercices pour la **voix**
  image: img/pjkbmdwz-1.png
  text: Lorem ipsum dolor sit amet ***consectetur adipisicing*** elit. Excepturi
    obcaecati nostrum impedit non rem architecto aut, modi ad cupiditate tempora
    aperiam sint maiores autem repellat voluptas, repellendus nobis alias omnis.
  button_bar:
    - type: external_link
      style: green
      to: patreon
      text: soutenez-moi sur Patreon
    - type: internal_scroll
      style: outline
      section_id: presentation
      text: voir Présentation
      icon: play
  nav:
    section_id: top
    text: Début de page
blocks:
  - type: section
    title: Apprenez
    image: img/hf2b4cfa-1.jpg
    text: |-
      * De **nombreux chants** et à **s'accompagner à la guitare**
      * Les **différents accords des chants** à partir de différentes tonalités
      * À **leader un bhajan**
      * Le sens des paroles
      * Des **rythmiques de guitare** spécifiques pour accompagner des bhajans
    button_bar:
      - type: internal_scroll
        section_id: concept
        text: en savoir plus
        style: green
    nav:
      section_id: apprenez
      text: Apprenez...
    invert: true
  - type: video
    title: Vidéo de présentation
    link: https://youtu.be/wKBdhskHUko
    image: img/maxresdefault.jpg
    nav:
      section_id: presentation
      text: Vidéo de présentation
  - type: section
    title: Explication du Concept
    image: img/zn38h7ea-1.png
    text: >-
      Minima ipsa, and fugiat, yet vitae. Duis cupidatat, and nequeporro. Sunt
      ratione, or ea, but in mollit fugit. Ut odit dolorem sunt, and fugit.
      Autem ad quae, but quaerat, and explicabo, for exercitation.\

      \

      Nequeporro id ullam, or ut, but aliquam elit, so dicta. Dicta aliquip. Lorem exercitationem, nor magna quaerat ab. Eaque. Error dolor, nesciunt. Natus. Elit ipsam, nor inventore si, so ratione, for consequat, or dicta.\

      \

      Aliquid sequi. Numquam. Unde. Aliquip numquam deserunt natus. Autem adipisicing, for incididunt, and iste, but eos, quaerat tempora. Eos. Quaerat fugit, or est, and duis, for ullamco. Vitae eaque, for unde, nor mollit. Nihil commodi explicabo commodo exercitationem. Ullamco quis. Quae. Adipisicing. Minima adipisci odit, nor dolore qui autem. Nequeporro dolores, for amet.
    nav:
      section_id: concept
      text: Explication du Concept
    button_bar:
      - type: external_link
        style: green
        to: patreon
        text: soutenez-moi sur Patreon
    invert: false
  - type: video
    title: Vidéo de tuto
    link: https://youtu.be/M-lKuAMzNkw
    image: img/maxresdefault2.jpg
    nav:
      section_id: tuto
      text: Vidéo de tuto
  - type: audio
    code: '<iframe width="100%" height="166" scrolling="no" frameborder="no"
      allow="autoplay"
      src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1243782397&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div
      style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break:
      normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;
      font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida
      Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a
      href="https://soundcloud.com/florian-surya-ananda" title="Florian Surya
      Ananda" target="_blank" style="color: #cccccc; text-decoration:
      none;">Florian Surya Ananda</a> · <a
      href="https://soundcloud.com/florian-surya-ananda/hari-om-tat-sat-nama-shivaya"
      title="hari om tat sat nama shivaya" target="_blank" style="color:
      #cccccc; text-decoration: none;">hari om tat sat nama shivaya</a></div>'
    title: Écoutez "hari om tat sat nama shivaya" sur Soundcloud
    nav:
      section_id: hari-om-tat-sat-nama-shivaya
      text: Écoutez "hari om tat sat nama shivaya" sur Soundcloud
  - type: testimonials
    testimonials:
      - text: >-
          Un grand moment de joie et de partage fraternel, plein de feu qui
          réanime notre coeur et nous donne l'envie de le diffuser.


          Surya met toute son énergie de dévotion au service du chant et ça vibre fort !


          Partager de si belles vibrations en cercle cela met le cœur en fête.
        name: Sylvie
      - text: >-
          Se retrouver pour chanter le Sacré, écouter, être traversée par les
          vibrations des sons venus du cœur.


          me connecter au Divin jusqu'à l'extase parfois.


          Je repars remplie de la joie, de l'espièglerie et de l'énergie solaire de Surya qui se donne à fond et avec beaucoup de générosité dans ses cercles.
        name: Carla
      - text: >-
          Les Bhajans qu’offrent Florian sont une magnifique occasion de chants
          et de connexions collectives.


          Chanter ensemble et vibrer de mille feux une joie et un amour sous différentes formes, nous rappeler qu’il est un espace éternel de joie et que cet espace est toujours en chacun de nous.


          Gratitude pour tes invitations Flow
        name: Yannis
    nav:
      section_id: temoignages
      text: Témoignages
    title: Témoignages
---
